import React from 'react';

class Headline extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <h1 className="headline_h1">It's the OTHER page!</h1>
        );
    }
}

export default Headline;