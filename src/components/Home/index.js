import React from 'react';

//importing of components inserted
import Headline from './Headline';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Headline />
        );
    }
}

export default Home;