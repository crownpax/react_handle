import React from 'react';

import './test.scss';

class Headline extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <h1 className="headline_h1">Hello, world!</h1>
        );
    }
}

export default Headline;