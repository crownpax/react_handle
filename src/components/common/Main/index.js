import React from 'react';
import { Switch, Route } from 'react-router-dom';

//importing of Main component styles
import './main.scss';

//importing of components inserted
import Home from '../../Home';
import About from '../../About';

class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <main className="main">
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/about" component={About}/>
                </Switch>
            </main>
        );
    }
}

export default Main;