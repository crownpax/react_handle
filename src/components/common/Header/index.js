import React from 'react';

//importing of Header component styles
import './header.scss';

//importing of components inserted
import Logo from './Logo';
import MainNavigationBar from './MainNavigationBar';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className="header">
                <Logo />
                <MainNavigationBar />
            </header>
        );
    }
}

export default Header;
