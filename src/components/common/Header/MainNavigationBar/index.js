import React from 'react';
import { NavLink } from 'react-router-dom';

import './main-navigation-bar.scss';

class MainNavigationBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav role="navigation" className="header__main-navigation-block">  
                <ul className="navigation-bar">
                    <li className="navigation-bar__item">
                        <NavLink exact to='/' className="navigation-bar__link" activeClassName="navigation-bar__link_active">Главная</NavLink>
                    </li>
                    <li className="navigation-bar__item">
                        <NavLink to='/about' className="navigation-bar__link" activeClassName="navigation-bar__link_active">О проекте</NavLink>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default MainNavigationBar;