import React from 'react';
import { Link } from 'react-router-dom';

//importing of Logo component styles
import './logo.scss';

class Logo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="logo-block">
                <Link to='/' className="logo-link">
                    <svg className="logo">
                        <text transform="matrix(1.1096 0 0 1 -1.1667 32.167)" className="logo__text">C</text>
                        <text transform="matrix(1.1096 0 0 1 24.9169 32.167)" className="logo__text">X</text>
                    </svg>
                </Link>
            </div>
        );
    }
}

export default Logo;