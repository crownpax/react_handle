import React from 'react';

//importing of main styles
import 'styles/style.scss';

//importing of components inserted
import Header from './common/Header';
import Main from './common/Main';

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <Header />
                <Main />
            </React.Fragment>
        );
    }
}

export default App;