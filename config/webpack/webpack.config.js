const path = require("path");
const webpack = require("webpack");

const publicPath = path.resolve();

module.exports = {
    entry: "./src/index.js",
    mode: "development",
    module: {
        rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules|bower_components)/,
            loader: "babel-loader",
            options: { presets: ["@babel/preset-env", "@babel/preset-react"] }
        },
        {
            test:  /\.s[ac]ss$/i,
            use: ['style-loader', 'css-loader',  'sass-loader'],
        }
        ]
    },
    resolve: { 
        extensions: ["*", ".js", ".jsx"],
        alias: {
            styles: path.join(publicPath, "src/styles/")
        } 
    },
    output: {
        filename: "bundle.js",
        path: path.resolve("dist/"),
        publicPath: "/dist/"
    },
    devServer: {
        contentBase: path.join(publicPath, "public/"),
        port: 3000,
        publicPath: "http://localhost:3000/dist/",
        hot: true,
        hotOnly: true
    },
    plugins: [new webpack.HotModuleReplacementPlugin()]
};